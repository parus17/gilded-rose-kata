package com.gildedrose.support;

import com.gildedrose.Item;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class ConjuredItemHandlerTest {
    private SpecializedItemHandler itemHandler = new ConjuredItemHandler();

    @Test
    public void canHandleItemFalse() {
        // Arrange
        Item item = new Item("Backstage passes to a TAFKAL80ETC concert", 1, 1);

        // Act & assert
        assertFalse(itemHandler.canHandleItem(item));
    }

    @Test
    public void canHandleItemTrue() {
        // Arrange
        Item item = new Item("Conjured Mana Cake", 1, 1);

        // Act & assert
        assertTrue(itemHandler.canHandleItem(item));
    }

    @Test
    public void doHandleItemWithSellIn4AndQuality4() {
        // Arrange
        Item item = new Item("Conjured Mana Cake", 4, 4);

        // Act
        itemHandler.doHandleItem(item);

        // Assert
        assertEquals(3, item.sellIn);
        assertEquals(2, item.quality);
    }

    @Test
    public void doHandleItemWithSellIn0AndQuality4() {
        // Arrange
        Item item = new Item("Conjured Mana Cake", 0, 4);

        // Act
        itemHandler.doHandleItem(item);

        // Assert
        assertEquals(-1, item.sellIn);
        assertEquals(0, item.quality);
    }

    @Test
    public void doHandleItemWithSellIn0AndQuality3() {
        // Arrange
        Item item = new Item("Conjured Mana Cake", 0, 4);

        // Act
        itemHandler.doHandleItem(item);

        // Assert
        assertEquals(-1, item.sellIn);
        assertEquals(0, item.quality);
    }
}