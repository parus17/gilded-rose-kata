package com.gildedrose.support;

import com.gildedrose.Item;
import org.junit.Test;

import static org.junit.Assert.*;

public class ItemHandlerFactoryTest {

    @Test
    public void handleConjuredItem() {
        // Arrange
        Item item = new Item("Conjured Mana Cake", 4, 4);

        // Act
        ItemHandlerFactory.handleItem(item);

        // Assert
        assertEquals(3, item.sellIn);
        assertEquals(2, item.quality);
    }
}