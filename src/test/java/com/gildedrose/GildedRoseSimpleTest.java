package com.gildedrose;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GildedRoseSimpleTest {
    private GildedRose gildedRose;

    @Test
    public void at_the_end_of_each_day_our_system_lowers_both_values_for_every_item() {
        // Arrange
        addAnItemToGildedRose("Item", 5, 5);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("Item", 4, 4);
    }

    @Test
    public void once_the_sell_by_date_has_passed_quality_degrades_twice_as_fast() {
        // Arrange
        addAnItemToGildedRose("Item", 0, 4);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("Item", -1, 2);
    }

    @Test
    public void the_quality_of_an_item_is_never_negative() {
        //_Arrange
        addAnItemToGildedRose("Item", 0, 0);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("Item", -1, 0);
    }

    /**
     * What if input of quality is already negative? Conclusion: no change, the value is not corrected.
     */
    @Test
    public void the_quality_of_an_item_is_never_negative_with_bad_input() {
        //_Arrange
        addAnItemToGildedRose("Item", 0, -1);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("Item", -1, -1);
    }

    @Test
    public void aged_brie_actually_increases_in_quality_the_older_it_gets() {
        //_Arrange
        addAnItemToGildedRose("Aged Brie", 5, 0);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("Aged Brie", 4, 1);
    }

    /**
     * What if input of quality is negative? Conclusion: aged brie recovers.
     */
    @Test
    public void aged_brie_actually_increases_in_quality_the_older_it_gets_with_bad_input() {
        //_Arrange
        addAnItemToGildedRose("Aged Brie", 5, -1);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("Aged Brie", 4, 0);
    }

    /**
     * Item is Aged Brie.
     */
    @Test
    public void the_quality_of_aged_brie_is_never_more_than_50() {
        // Arrange
        addAnItemToGildedRose("Aged Brie", 5, 50);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("Aged Brie", 4, 50);
    }

    /**
     * Item is Aged Brie.
     * What if input of quality is already more then 50? Conclusion: no change, the value is not corrected.
     */
    @Test
    public void the_quality_of_aged_brie_is_never_more_than_50_with_bad_input() {
        // Arrange
        addAnItemToGildedRose("Aged Brie", 5, 51);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("Aged Brie", 4, 51);
    }

    /**
     * Item is anything but Aged Brie.
     */
    @Test
    public void the_quality_of_an_item_is_never_more_than_50() {
        // Arrange
        addAnItemToGildedRose("Item", 5, 50);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("Item", 4, 49);
    }

    /**
     * Item is anything but Aged Brie.
     * What if input of quality is already more then 50? Conclusion: the value decreases.
     */
    @Test
    public void the_quality_of_an_item_is_never_more_than_50_with_bad_input() {
        // Arrange
        addAnItemToGildedRose("Item", 5, 52);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("Item", 4, 51);
    }

    @Test
    public void sulfuras_being_a_legendary_item_never_has_to_be_sold_or_decreases_in_quality() {
        // Arrange
        addAnItemToGildedRose("Sulfuras, Hand of Ragnaros", 5, 80);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("Sulfuras, Hand of Ragnaros", 5, 80);
    }

    @Test
    public void backstage_passes_like_aged_brie_increases_in_quality_as_its_sellIn_value_approaches() {
        // Arrange
        addABackstatePassesItemToGildedRose(12, 0);

        // Act & assert
        updateQualityAndVerifyTheBackstagePassesItem(11, 1);
        updateQualityAndVerifyTheBackstagePassesItem(10, 2);
        updateQualityAndVerifyTheBackstagePassesItem(9, 4);
        updateQualityAndVerifyTheBackstagePassesItem(8, 6);
        updateQualityAndVerifyTheBackstagePassesItem(7, 8);
        updateQualityAndVerifyTheBackstagePassesItem(6, 10);
        updateQualityAndVerifyTheBackstagePassesItem(5, 12);
        updateQualityAndVerifyTheBackstagePassesItem(4, 15);
        updateQualityAndVerifyTheBackstagePassesItem(3, 18);
        updateQualityAndVerifyTheBackstagePassesItem(2, 21);
        updateQualityAndVerifyTheBackstagePassesItem(1, 24);
        updateQualityAndVerifyTheBackstagePassesItem(0, 27);
        updateQualityAndVerifyTheBackstagePassesItem(-1, 0);
        updateQualityAndVerifyTheBackstagePassesItem(-2, 0);
    }

    @Test(expected = NullPointerException.class)
    public void special_case_where_item_name_is_null() {
        // Arrange
        addAnItemToGildedRose(null, 5, 5);

        // Act
        updateQuality();
    }

    @Test
    public void special_case_where_item_name_is_empty() {
        // Arrange
        addAnItemToGildedRose("", 5, 5);

        // Act
        updateQuality();

        // Assert
        verifyTheItem("", 4, 4);
    }

    @Test(expected = NullPointerException.class)
    public void special_case_where_items_is_null() {
        // Arrange
        gildedRose = new GildedRose(null);

        // Act
        updateQuality();

    }

    private void addABackstatePassesItemToGildedRose(int sellIn, int quality) {
        Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert", sellIn, quality)};
        gildedRose = new GildedRose(items);

    }

    private void addAnItemToGildedRose(String name, int sellIn, int quality) {
        Item[] items = new Item[]{new Item(name, sellIn, quality)};
        gildedRose = new GildedRose(items);

    }

    private void updateQuality() {
        gildedRose.updateQuality();
    }

    private void updateQualityAndVerifyTheBackstagePassesItem(int i, int i2) {
        // Act
        updateQuality();

        // Assert
        verifyTheItem("Backstage passes to a TAFKAL80ETC concert", i, i2);
    }

    private void verifyTheItem(String name, int sellIn, int quality) {
        Item modifiedItem = gildedRose.items[0];
        assertEquals(name, modifiedItem.name);
        assertEquals(sellIn, modifiedItem.sellIn);
        assertEquals(quality, modifiedItem.quality);
    }


}
