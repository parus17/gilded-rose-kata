package com.gildedrose.support;

import com.gildedrose.Item;

import static com.gildedrose.support.ItemQuery.qualityIsNotMoreThen50;
import static com.gildedrose.support.ItemQuery.qualityIsNotNegative;

public class ItemCommand {
    private ItemCommand() {
        //
    }

    public static void decreaseQualityByOne(Item item) {
        item.quality = item.quality - 1;
    }

    public static void decreaseSellInByOne(Item item) {
        item.sellIn = item.sellIn - 1;
    }

    public static void increaseQualityByOne(Item item) {
        item.quality = item.quality + 1;
    }

    public static void tryToDecreaseQualityBy(Item item, int delta) {
        int counter = 0;
        while (counter++ < delta) {
            tryToDecreaseQualityByOne(item);
        }
    }

    public static void tryToDecreaseQualityByOne(Item item) {
        if (qualityIsNotNegative(item)) {
            decreaseQualityByOne(item);
        }
    }

    public static void tryToIncreaseQualityByOne(Item item) {
        if (qualityIsNotMoreThen50(item)) {
            increaseQualityByOne(item);
        }
    }

}
