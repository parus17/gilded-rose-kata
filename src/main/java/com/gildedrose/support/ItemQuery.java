package com.gildedrose.support;

import com.gildedrose.Item;

public class ItemQuery {
    private ItemQuery() {
        //
    }
    
    public static boolean qualityIsNotMoreThen50(Item item) {
        return item.quality < 50;
    }

    public static boolean qualityIsNotNegative(Item item) {
        return item.quality > 0;
    }

    public static boolean sellByDateHasPassed(Item item) {
        return item.sellIn < 0;
    }

    public static boolean sellIn5DaysOrLess(Item item) {
        return item.sellIn <= 5;
    }

    public static boolean sellIn10DaysOrLess(Item item) {
        return item.sellIn <= 10;
    }
}
