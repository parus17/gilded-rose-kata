package com.gildedrose.support;

import com.gildedrose.Item;

public class SulfurasItemHandler implements SpecializedItemHandler {
    @Override
    public boolean canHandleItem(Item item) {
        return item.name.equals("Sulfuras, Hand of Ragnaros");
    }

    @Override
    public void doHandleItem(Item item) {
        //
    }
}
