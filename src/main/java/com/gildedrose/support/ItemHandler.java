package com.gildedrose.support;

import com.gildedrose.Item;

public interface ItemHandler {
    void doHandleItem(Item item);
}
