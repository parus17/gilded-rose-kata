package com.gildedrose.support;

import com.gildedrose.Item;

import static com.gildedrose.support.ItemCommand.*;
import static com.gildedrose.support.ItemQuery.*;

public class BackstagePassesItemHandler implements SpecializedItemHandler {
    @Override
    public boolean canHandleItem(Item item) {
        return item.name.equals("Backstage passes to a TAFKAL80ETC concert");
    }

    @Override
    public void doHandleItem(Item item) {
        if (qualityIsNotMoreThen50(item)) {
            increaseQualityByOne(item);
            if (sellIn10DaysOrLess(item)) {
                tryToIncreaseQualityByOne(item);
            }
            if (sellIn5DaysOrLess(item)) {
                tryToIncreaseQualityByOne(item);
            }
        }
        //
        decreaseSellInByOne(item);
        //
        if (sellByDateHasPassed(item)) {
            item.quality = 0;
        }
    }
}
