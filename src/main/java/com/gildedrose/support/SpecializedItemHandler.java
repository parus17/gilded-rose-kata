package com.gildedrose.support;

import com.gildedrose.Item;

public interface SpecializedItemHandler extends ItemHandler {
    boolean canHandleItem(Item item);
}
