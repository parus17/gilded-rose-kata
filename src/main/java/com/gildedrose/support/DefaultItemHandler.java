package com.gildedrose.support;

import com.gildedrose.Item;

import static com.gildedrose.support.ItemCommand.decreaseSellInByOne;
import static com.gildedrose.support.ItemCommand.tryToDecreaseQualityByOne;
import static com.gildedrose.support.ItemQuery.sellByDateHasPassed;

public class DefaultItemHandler implements ItemHandler {
    @Override
    public void doHandleItem(Item item) {
        tryToDecreaseQualityByOne(item);
        //
        decreaseSellInByOne(item);
        //
        if (sellByDateHasPassed(item)) {
            tryToDecreaseQualityByOne(item);
        }
    }
}
