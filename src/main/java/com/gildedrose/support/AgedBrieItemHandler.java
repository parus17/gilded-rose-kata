package com.gildedrose.support;

import com.gildedrose.Item;

import static com.gildedrose.support.ItemCommand.decreaseSellInByOne;
import static com.gildedrose.support.ItemCommand.tryToIncreaseQualityByOne;
import static com.gildedrose.support.ItemQuery.sellByDateHasPassed;

public class AgedBrieItemHandler implements SpecializedItemHandler {
    @Override
    public boolean canHandleItem(Item item) {
        return item.name.equals("Aged Brie");
    }

    @Override
    public void doHandleItem(Item item) {
        tryToIncreaseQualityByOne(item);
        //
        decreaseSellInByOne(item);
        //
        if (sellByDateHasPassed(item)) {
            tryToIncreaseQualityByOne(item);
        }
    }
}
