package com.gildedrose.support;

import com.gildedrose.Item;

import static com.gildedrose.support.ItemCommand.decreaseSellInByOne;
import static com.gildedrose.support.ItemCommand.tryToDecreaseQualityBy;
import static com.gildedrose.support.ItemQuery.sellByDateHasPassed;

public class ConjuredItemHandler implements SpecializedItemHandler {
    @Override
    public boolean canHandleItem(Item item) {
        return item.name.equals("Conjured Mana Cake");
    }

    @Override
    public void doHandleItem(Item item) {
        tryToDecreaseQualityBy(item, 2);
        //
        decreaseSellInByOne(item);
        //
        if (sellByDateHasPassed(item)) {
            tryToDecreaseQualityBy(item, 2);
        }
    }
}
