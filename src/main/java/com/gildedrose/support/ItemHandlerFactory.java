package com.gildedrose.support;

import com.gildedrose.Item;

import java.util.Arrays;
import java.util.List;

public class ItemHandlerFactory {
    private static ItemHandler DEFAULT_ITEM_HANDLER = new DefaultItemHandler();
    private static List<SpecializedItemHandler> SPECIALIZED_ITEM_HANDLERS;

    private ItemHandlerFactory() {
        //
    }

    static {
        SPECIALIZED_ITEM_HANDLERS = Arrays.asList(
                new AgedBrieItemHandler(),
                new ConjuredItemHandler(),
                new BackstagePassesItemHandler(),
                new SulfurasItemHandler()
        );
    }


    public static void handleItem(Item item) {
        SPECIALIZED_ITEM_HANDLERS.stream()
                .filter(handler -> handler.canHandleItem(item))
                .findFirst()
                .map(handler -> (ItemHandler) handler)
                .orElse(DEFAULT_ITEM_HANDLER)
                .doHandleItem(item);
    }
}
