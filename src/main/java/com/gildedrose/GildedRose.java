package com.gildedrose;

import com.gildedrose.support.ItemHandlerFactory;

import static com.gildedrose.support.ItemHandlingCommand.*;
import static com.gildedrose.support.ItemQuery.*;


class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            ItemHandlerFactory.handleItem(item);
//            if (isOfAgedBrieCategory(item)) {
//                handleAgedBrieItem(item);
//            } else if (isOfSulfurasCategory(item)) {
//                handleSulfurasItem(item);
//            } else if (isOfBackstagePassesCategory(item)) {
//                handleBackstagePassesItem(item);
//            } else {
//                handleDefaultItem(item);
//            }
        }
    }
}